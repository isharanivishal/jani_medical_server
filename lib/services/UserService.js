"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const typeorm_1 = require("typeorm");
const typeorm_typedi_extensions_1 = require("typeorm-typedi-extensions");
const Users_1 = require("../entities/Users");
let UserService = class UserService {
    constructor() {
    }
    // create new code
    createCode(code, name, role = 1) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = new Users_1.Users();
            user.role = role;
            user.code = code ? code : Math.random().toString(36).substr(2, 5);
            user.name = name;
            return this.userRepo.persist(user);
        });
    }
    // find code which is alive
    findUserByCode(code) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.userRepo.findOne({ code, isAlive: true });
        });
    }
    addNewUser(id, deviceId) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = yield this.userRepo.findOneById(id);
            user.deviceId = deviceId;
            user.isAlive = true;
            return this.userRepo.persist(user);
        });
    }
    // make a code dead
    deadACode(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = yield this.userRepo.findOneById(id);
            user.isAlive = false;
            return this.userRepo.persist(user);
        });
    }
};
__decorate([
    typeorm_typedi_extensions_1.OrmRepository(Users_1.Users),
    __metadata("design:type", typeorm_1.Repository)
], UserService.prototype, "userRepo", void 0);
UserService = __decorate([
    typedi_1.Service(),
    __metadata("design:paramtypes", [])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=UserService.js.map