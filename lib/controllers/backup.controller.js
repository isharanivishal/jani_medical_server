"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const routing_controllers_1 = require("routing-controllers");
const typedi_1 = require("typedi");
const path = require("path");
const fs = require("fs");
const UserService_1 = require("../services/UserService");
let BackupController = class BackupController {
    constructor() {
        this.backupDir = path.join(__dirname, '..', '..', 'backup');
        try {
            fs.accessSync(this.backupDir);
        }
        catch (e) {
            fs.mkdirSync(this.backupDir);
        }
    }
    saveBackup(query) {
        if (query.data && query.deviceId) {
            // replace file
            let fileName = `${this.backupDir}/${query.deviceId}.json`;
            // make new file
            query.data = JSON.stringify(query.data);
            try {
                fs.writeFileSync(fileName, query.data);
                return 'Backup Taken Successfully';
            }
            catch (e) {
                throw new routing_controllers_1.InternalServerError('Something went wrong');
            }
        }
        else {
            throw new routing_controllers_1.BadRequestError('Something went wrong');
        }
    }
    readFile(query, response) {
        if (query.deviceId) {
            let fileName = `${this.backupDir}/${query.deviceId}.json`;
            try {
                return JSON.parse(fs.readFileSync(fileName, 'utf8'));
            }
            catch (e) {
                throw new routing_controllers_1.NotFoundError('File not found');
            }
        }
        else {
            throw new routing_controllers_1.BadRequestError('Somehting went wrong');
        }
    }
};
__decorate([
    typedi_1.Inject(),
    __metadata("design:type", UserService_1.UserService)
], BackupController.prototype, "_userService", void 0);
__decorate([
    routing_controllers_1.Post('/backup'),
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], BackupController.prototype, "saveBackup", null);
__decorate([
    routing_controllers_1.Post('/restorebackup'),
    __param(0, routing_controllers_1.Body()), __param(1, routing_controllers_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], BackupController.prototype, "readFile", null);
BackupController = __decorate([
    routing_controllers_1.JsonController(),
    __metadata("design:paramtypes", [])
], BackupController);
exports.BackupController = BackupController;
//# sourceMappingURL=backup.controller.js.map