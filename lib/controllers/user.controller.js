"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const routing_controllers_1 = require("routing-controllers");
const typedi_1 = require("typedi");
const UserService_1 = require("../services/UserService");
let UserController = class UserController {
    constructor() {
    }
    /**
     * create new code
     * @param query
     */
    createCode(query) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = yield this._userService.createCode(query.code, query.name, query.role);
            if (user) {
                return user;
            }
            else {
                throw new routing_controllers_1.BadRequestError('you have created bad request');
            }
        });
    }
    /**
     * check code
     * @param query
     */
    chcekCode(query) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = yield this._userService.findUserByCode(query.code);
            // chek if user 
            if (user) {
                if (query.deviceId && user.deviceId === query.deviceId) {
                    // code is right and device is same
                    return user;
                }
                else if (query.deviceId && !user.deviceId) {
                    // deviceId is null means new user
                    return yield this._userService.addNewUser(user.id, query.deviceId);
                }
                else {
                    // extra efforts from client
                    throw new routing_controllers_1.NotFoundError('you have enterd wrong code');
                }
            }
            else {
                // wrong code
                throw new routing_controllers_1.NotFoundError('you have enterd wrong code');
            }
        });
    }
    /**
     * update code
     * @param code
     */
    updateCode(query) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = yield this._userService.findUserByCode(query.code);
            // chek if user 
            if (user) {
                if (user.deviceId !== null && user.deviceId === query.deviceId) {
                    // code is right and device is same
                    return user;
                }
                else if (!user.deviceId) {
                    // deviceId is null means new user
                    return yield this._userService.addNewUser(user.id, query.deviceId);
                }
                else {
                    // extra efforts from client
                    throw new routing_controllers_1.NotFoundError('you have enterd wrong code');
                }
            }
            else {
                // wrong code
                throw new routing_controllers_1.NotFoundError('you have enterd wrong code');
            }
        });
    }
};
__decorate([
    typedi_1.Inject(),
    __metadata("design:type", UserService_1.UserService)
], UserController.prototype, "_userService", void 0);
__decorate([
    routing_controllers_1.Post('/createcode'),
    __param(0, routing_controllers_1.Body({ required: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "createCode", null);
__decorate([
    routing_controllers_1.Post('/code'),
    __param(0, routing_controllers_1.Body({ required: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "chcekCode", null);
__decorate([
    routing_controllers_1.Post('/updatecode'),
    __param(0, routing_controllers_1.Body({ required: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "updateCode", null);
UserController = __decorate([
    routing_controllers_1.JsonController(),
    __metadata("design:paramtypes", [])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map