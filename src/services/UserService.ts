import { Service } from 'typedi';
import { Repository } from 'typeorm';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { BadRequestError, NotFoundError } from 'routing-controllers';

import { Users } from '../entities/Users'

@Service()
export class UserService {
    @OrmRepository(Users)
    private userRepo: Repository<Users>;

    constructor() {

    }

    // create new code
    async createCode(code: string, name: string, role: number = 1) {
        let user = new Users();
        user.role = role;
        user.code = code ? code : Math.random().toString(36).substr(2, 5);
        user.name = name;
        return this.userRepo.persist(user);
    }

    // find code which is alive
    async findUserByCode(code: string): Promise<Users> {
        return this.userRepo.findOne({ code, isAlive: true });
    }

    async addNewUser(id: number, deviceId) {
        let user = await this.userRepo.findOneById(id);
        user.deviceId = deviceId
        user.isAlive = true
        return this.userRepo.persist(user)
    }

    // make a code dead
    async deadACode(id: number) {
        let user = await this.userRepo.findOneById(id);
        user.isAlive = false
        return this.userRepo.persist(user)
    }
}