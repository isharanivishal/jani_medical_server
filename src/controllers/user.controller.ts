import { JsonController, Body, BodyParam, Post, NotFoundError, InternalServerError, BadRequestError } from 'routing-controllers';
import { Inject } from 'typedi';

import { Users } from '../entities/Users';
import { UserService } from '../services/UserService';

@JsonController()
export class UserController {

    @Inject()
    private _userService: UserService;

    constructor() {

    }

    /**
     * create new code
     * @param query 
     */
    @Post('/createcode')
    async createCode( @Body({ required: true }) query: { code: string, name: string, role: number }) {
        let user = await this._userService.createCode(query.code, query.name, query.role);

        if (user) {
            return user;
        } else {
            throw new BadRequestError('you have created bad request');
        }
    }

    /**
     * check code
     * @param query 
     */
    @Post('/code')
    async chcekCode( @Body({ required: true }) query: { code: string, deviceId: string }) {
        let user = await this._userService.findUserByCode(query.code);

        // chek if user 
        if (user) {

            if (query.deviceId && user.deviceId === query.deviceId) {
                // code is right and device is same
                return user;
            } else if (query.deviceId && !user.deviceId) {
                // deviceId is null means new user
                return await this._userService.addNewUser(user.id, query.deviceId);
            } else {
                // extra efforts from client
                throw new NotFoundError('you have enterd wrong code');
            }

        } else {
            // wrong code
            throw new NotFoundError('you have enterd wrong code');
        }
    }

    /**
     * update code
     * @param code 
     */
    @Post('/updatecode')
    async updateCode( @Body({ required: true }) query: { code: string, deviceId: string }) {
        let user = await this._userService.findUserByCode(query.code);

        // chek if user 
        if (user) {

            if (user.deviceId !== null && user.deviceId === query.deviceId) {
                // code is right and device is same
                return user;
            } else if (!user.deviceId) {
                // deviceId is null means new user
                return await this._userService.addNewUser(user.id, query.deviceId);
            } else {
                // extra efforts from client
                throw new NotFoundError('you have enterd wrong code');
            }

        } else {
            // wrong code
            throw new NotFoundError('you have enterd wrong code');
        }
    }
}