import { JsonController, Body, BodyParam, Post, NotFoundError, InternalServerError, BadRequestError, UploadedFile, ContentType, Req, Res, Controller } from 'routing-controllers';
import { Inject } from 'typedi';
import * as path from 'path';
import * as fs from 'fs';

import { Users } from '../entities/Users';
import { UserService } from '../services/UserService';

@JsonController()
export class BackupController {

    @Inject()
    private _userService: UserService;

    backupDir: string;

    constructor() {
        this.backupDir = path.join(__dirname, '..', '..', 'backup')
        try {
            fs.accessSync(this.backupDir);
        } catch (e) {
            fs.mkdirSync(this.backupDir);
        }
    }

    @Post('/backup')
    saveBackup(@Body() query: {data: any,deviceId: string}) {
        if(query.data && query.deviceId) {
            // replace file
            let fileName = `${this.backupDir}/${query.deviceId}.json`;
            // make new file
            query.data = JSON.stringify(query.data);
            try {
                fs.writeFileSync(fileName,query.data);
                return 'Backup Taken Successfully';
            } catch (e) {
                throw new InternalServerError('Something went wrong');
            }
        } else {
            throw new BadRequestError('Something went wrong');
        }
    }

    @Post('/restorebackup')
    readFile(@Body() query: { deviceId: string }, @Res() response: any) {
        if(query.deviceId) {
            let fileName = `${this.backupDir}/${query.deviceId}.json`;
            try {
                return JSON.parse(fs.readFileSync(fileName, 'utf8'));
            } catch (e) {
                throw new NotFoundError('File not found');
            }
        } else {
            throw new BadRequestError('Somehting went wrong');
        }
    }
    
}