import "reflect-metadata";
import "source-map-support/register";

import * as express from 'express';
import * as path from 'path';
import * as config from 'config';
import * as log4js from 'log4js';
import * as bodyParser from 'body-parser';

import { createConnection, useContainer as useContainerForOrm } from 'typeorm';
import { Container } from 'typedi';
import { createExpressServer, useExpressServer, useContainer as useContainerForRouting } from 'routing-controllers';

const packageJson = require('../package.json');

const dbConfig = {
    type: "mysql",
    host: process.env.OPENSHIFT_MYSQL_DB_HOST,
    port: process.env.OPENSHIFT_MYSQL_DB_PORT,
    username: process.env.OPENSHIFT_MYSQL_DB_USERNAME,
    password: process.env.OPENSHIFT_MYSQL_DB_PASSWORD,
    database: "janimedicalserver"
}

// configure logs
log4js.configure(config['log4js']);

// use container
useContainerForOrm(Container);
useContainerForRouting(Container);

const expressApp = express();
expressApp.use(bodyParser({ limit: '50mb' }));
expressApp.use(bodyParser.urlencoded({limit: '50mb'}));


// create connection 
createConnection({
    driver: {
        type: "mysql",
        host: process.env.OPENSHIFT_MYSQL_DB_HOST || 'localhost',
        port: process.env.OPENSHIFT_MYSQL_DB_PORT || '3306',
        username: process.env.OPENSHIFT_MYSQL_DB_USERNAME || 'root',
        password: process.env.OPENSHIFT_MYSQL_DB_PASSWORD || '',
        database: "janimedicalserver"
    },
    logging: {
        logger: (level, message) => log4js.getLogger('debug').debug(message),
        logQueries: true,
        logFailedQueryError: true
    },
    entities: [
        `${__dirname}/entities/{*.ts,*.js}`
    ],
    autoSchemaSync: true
}).then(() => {
    const app = useExpressServer(expressApp, {
        cors: true,
        routePrefix: '/api',
        controllers: [`${__dirname}/controllers/{*.ts,*.js}`]
    })

    app.use(log4js.connectLogger(log4js.getLogger('access'), {
        level: 'auto',
        nolog: config['noaccesslog'],
    }));

    app.listen(process.env.OPENSHIFT_NODEJS_PORT || 4000, process.env.OPENSHIFT_NODEJS_IP);
    log4js.getLogger('debug').info(`${packageJson['name']}@${packageJson['version']} started`);
}).catch((e) => log4js.getLogger('error').error(e));